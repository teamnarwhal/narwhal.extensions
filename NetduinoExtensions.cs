//----------------------------------------------------------------------------
// Copyright � 2013 The Narwhal Group, All Rights Reserved
//
// PROPRIETARY
// THIS SOURCE CODE IS THE PROPERTY OF THE NARWHAL GROUP. IT MAY BE USED BY 
// RECIPIENT ONLY FOR THE PURPOSE FOR WHICH IT WAS TRANSMITTED AND MUST BE 
// RETURNED UPON REQUEST OR WHEN NO LONGER NEEDED BY RECIPIENT. IT MAY NOT BE 
// COPIED OR COMMUNICATED WITHOUT THE WRITTEN CONSENT OF THE NARWHAL GROUP.
//-----------------------------------------------------------------------------
using System.Collections;
using Microsoft.SPOT.Hardware;

namespace Narwhal.Extensions
{
    public class NetduinoPinMapper
    {
        Hashtable analogPins = new Hashtable();
        Hashtable digitalPins = new Hashtable();
        NetduinoPinMapper()
        {
            // map digital pins
            digitalPins[Cpu.Pin.GPIO_NONE] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_NONE;
            digitalPins[Cpu.Pin.GPIO_Pin0] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D0;
            digitalPins[Cpu.Pin.GPIO_Pin1] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D1;
            digitalPins[Cpu.Pin.GPIO_Pin2] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D2;
            digitalPins[Cpu.Pin.GPIO_Pin3] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D3;
            digitalPins[Cpu.Pin.GPIO_Pin4] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D4;
            digitalPins[Cpu.Pin.GPIO_Pin5] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D5;
            digitalPins[Cpu.Pin.GPIO_Pin6] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D6;
            digitalPins[Cpu.Pin.GPIO_Pin7] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D7;
            digitalPins[Cpu.Pin.GPIO_Pin8] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D8;
            digitalPins[Cpu.Pin.GPIO_Pin9] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D9;
            digitalPins[Cpu.Pin.GPIO_Pin10] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D10;
            digitalPins[Cpu.Pin.GPIO_Pin11] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D11;
            digitalPins[Cpu.Pin.GPIO_Pin12] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D12;
            digitalPins[Cpu.Pin.GPIO_Pin13] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_PIN_D13;
            digitalPins[Cpu.Pin.GPIO_Pin14] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_NONE;
            digitalPins[Cpu.Pin.GPIO_Pin15] = SecretLabs.NETMF.Hardware.Netduino.Pins.GPIO_NONE;

            // map analog pins
            analogPins[Cpu.AnalogChannel.ANALOG_NONE] = Cpu.AnalogChannel.ANALOG_NONE;
            analogPins[Cpu.AnalogChannel.ANALOG_0] = SecretLabs.NETMF.Hardware.Netduino.AnalogChannels.ANALOG_PIN_A0;
            analogPins[Cpu.AnalogChannel.ANALOG_1] = SecretLabs.NETMF.Hardware.Netduino.AnalogChannels.ANALOG_PIN_A1;
            analogPins[Cpu.AnalogChannel.ANALOG_2] = SecretLabs.NETMF.Hardware.Netduino.AnalogChannels.ANALOG_PIN_A2;
            analogPins[Cpu.AnalogChannel.ANALOG_3] = SecretLabs.NETMF.Hardware.Netduino.AnalogChannels.ANALOG_PIN_A3;
            analogPins[Cpu.AnalogChannel.ANALOG_4] = SecretLabs.NETMF.Hardware.Netduino.AnalogChannels.ANALOG_PIN_A4;
            analogPins[Cpu.AnalogChannel.ANALOG_5] = SecretLabs.NETMF.Hardware.Netduino.AnalogChannels.ANALOG_PIN_A5;
            analogPins[Cpu.AnalogChannel.ANALOG_6] = Cpu.AnalogChannel.ANALOG_NONE;
            analogPins[Cpu.AnalogChannel.ANALOG_7] = Cpu.AnalogChannel.ANALOG_NONE;
        }
        static NetduinoPinMapper instance = new NetduinoPinMapper();

        Cpu.Pin digitalMap(Cpu.Pin pin) { return (Cpu.Pin)digitalPins[pin]; }
        Cpu.AnalogChannel analogMap(Cpu.AnalogChannel pin) { return (Cpu.AnalogChannel)analogPins[pin]; }

        public static Cpu.Pin DigitalMap(Cpu.Pin pin) { return instance.digitalMap(pin); }
        public static Cpu.Pin DigitalMap(int pin) { return instance.digitalMap((Cpu.Pin)pin); }
        public static Cpu.AnalogChannel AnalogMap(Cpu.AnalogChannel pin) { return instance.analogMap(pin); }
        public static Cpu.AnalogChannel AnalogMap(int pin) { return AnalogMap((Cpu.AnalogChannel)pin); }
    }
}
