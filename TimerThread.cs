//----------------------------------------------------------------------------
// Copyright � 2013 The Narwhal Group, All Rights Reserved
//
// PROPRIETARY
// THIS SOURCE CODE IS THE PROPERTY OF THE NARWHAL GROUP. IT MAY BE USED BY 
// RECIPIENT ONLY FOR THE PURPOSE FOR WHICH IT WAS TRANSMITTED AND MUST BE 
// RETURNED UPON REQUEST OR WHEN NO LONGER NEEDED BY RECIPIENT. IT MAY NOT BE 
// COPIED OR COMMUNICATED WITHOUT THE WRITTEN CONSENT OF THE NARWHAL GROUP.
//-----------------------------------------------------------------------------
using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Net.NetworkInformation;
using Narwhal.Extensions;

namespace Narwhal.Extensions
{
    /// <summary>
    /// Encapsulates arguments passed when an event timer elapses.
    /// </summary>
    public sealed class ElapsedEventArgs : EventArgs
    {
        /// <summary>
        /// UTC time when event elapsed.
        /// </summary>
        public DateTime SignalTime { private set; get; }

        /// <summary>
        /// Constructor for object.
        /// </summary>
        /// <param name="signalTime">UTC time when event elapsed</param>
        public ElapsedEventArgs(DateTime signalTime)
        {
            SignalTime = signalTime;
        }
    }

    /// <summary>
    /// The delegate fired when the timer goes off.
    /// </summary>
    /// <param name="sender">object that fired the event</param>
    /// <param name="e">event arguments</param>
    public delegate void ElapsedEventHandler(object sender, ElapsedEventArgs e);

    /// <summary>
    /// Thread class that also encapsulates a timer.
    /// </summary>
    /// <remarks>Use this class when you need to do something periodically and it can be easily scheduled via a timer.</remarks>
    public class TimerThread : IDisposable
    {
        #region public methods

        /// <summary>
        /// Start the thread.
        /// </summary>
        public void Start()
        {
            lock (synclock)
            {
                startThread();
            }
        }

        /// <summary>
        /// Stop (suspend) the thread.
        /// </summary>
        public void Stop()
        {
            lock (synclock)
            {
                stopThread();
            }
        }

        /// <summary>
        /// Kill the thread and create a new one.
        /// </summary>
        public void Reset()
        {
            Debug.Print("Resetting thread " + mainThread.ManagedThreadId
                + ", IsAlive " + mainThread.IsAlive
                + ", ThreadState " + mainThread.ThreadState);
            lock (synclock)
            {
                resetThread();
            }
        }

        #endregion

        #region public properties

        public int TimerInterval { get; set; }
        public bool IsAligned { get; set; }

        #endregion

        #region public events

        /// <summary>
        /// This events fires if none of the configured servers respond successfully.
        /// </summary>
        public event ElapsedEventHandler Elapsed;

        #endregion

        #region protected constructor

        // event for timer thread
        ManualResetEvent timerEvent = new ManualResetEvent(true);

        // the thread
        Thread mainThread;

        // event to control stopping thread
        ManualResetEvent mainThreadStopEvent = new ManualResetEvent(false);

        // event for timer
        readonly TimeSpan noPeriod = new TimeSpan(0, 0, 0, 0, Timeout.Infinite);

        // private ctor
        protected TimerThread()
        {
            // default to aligned to TOD boundaries
            IsAligned = true;

            // default timer interval is infinite
            TimerInterval = Timeout.Infinite;

            // create the new thread
            createThread();
        }

        #endregion

        #region protected methods

        protected void WakeThread()
        {
            timerEvent.Set();
        }

        #endregion

        #region private methods

        // singleton instance
        object synclock = new object();

        #region thread methods

        void createThread()
        {
            if (mainThread != null)
                throw new InvalidOperationException("Cannot create a second thread, reset the existing thread first");

            // reset the manual reset event
            mainThreadStopEvent.Reset();

            // create a new thread
            mainThread = new Thread(new ThreadStart(mainThreadLoop));
            Debug.Print("Created new thread " + mainThread.ManagedThreadId
                + ", IsAlive " + mainThread.IsAlive
                + ", ThreadState " + mainThread.ThreadState);
        }

        void startThread()
        {
            if (mainThread == null)
                createThread();

            Debug.Print("Starting thread " + mainThread.ManagedThreadId
                + ", IsAlive " + mainThread.IsAlive
                + ", ThreadState " + mainThread.ThreadState);
            mainThread.Start();
        }

        /// <summary>
        /// Kill the current thread.
        /// </summary>
        void stopThread()
        {
            Debug.Print("Stopping thread " + mainThread.ManagedThreadId
                + ", IsAlive " + mainThread.IsAlive
                + ", ThreadState " + mainThread.ThreadState);

            // ask the thread to stop
            mainThreadStopEvent.Set();

            // join or kill the thread
            try
            {
                // wait for the thread to exit
                mainThread.Join();
                Debug.Print("Thread " + mainThread.ManagedThreadId + " stopped");
            }
            catch (Exception e)
            {
                Debug.Print("Thread.Join() failed: " + e.Message);
            }
            finally
            {
                mainThread = null;
            }
        }

        /// <summary>
        /// Kill the thread and create a new one.
        /// </summary>
        void resetThread()
        {
            Debug.Print("Resetting thread " + mainThread.ManagedThreadId
                + ", IsAlive " + mainThread.IsAlive
                + ", ThreadState " + mainThread.ThreadState);

            // stop the thread
            stopThread();

            // create the new thread
            createThread();
        }

        /// <summary>
        /// Thread loop where timer is managed.
        /// </summary>
        void mainThreadLoop()
        {
            // create the timer but don't do anything with it
            var timer = new Timer((object state) =>
            {
                (state as ManualResetEvent).Set();
            }
            , timerEvent, Timeout.Infinite, Timeout.Infinite);

            try
            {
                // wait for something
                for (var whichEvent = WaitHandle.WaitAny(new WaitHandle[] { mainThreadStopEvent, timerEvent });
                    whichEvent != 0;
                    whichEvent = WaitHandle.WaitAny(new WaitHandle[] { mainThreadStopEvent, timerEvent }))
                {
                    // reset the timer
                    timerEvent.Reset();

                    // call the elapsed method
                    if (Elapsed != null)
                        Elapsed(this, new ElapsedEventArgs(DateTime.Now));

                    var resetTime = (IsAligned && TimerInterval > 0)
                        ? TimeSpan.FromTicks(DateTime.Now.Ticks).TimespanToNextInterval(TimerInterval)
                        : new TimeSpan(0, 0, 0, 0, TimerInterval);

                    // reset the timer
                    timer.Change(resetTime, noPeriod);
                }
            }
            catch (ThreadAbortException) { }
            finally
            {
                // kill the timer
                if (timer != null)
                {
                    timer.Change(Timeout.Infinite, 0);
                    timer.Dispose();
                }
            }
        }

        #endregion

        #endregion

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }

    public sealed class NetworkInformationEventArgs : EventArgs
    {
        public string IpAddress { private set; get; }
        public bool IsAvailable { private set; get; }
        internal NetworkInformationEventArgs(bool isAvailable, string ipAddress)
        {
            IsAvailable = isAvailable;
            IpAddress = ipAddress;
        }
    }

    public delegate void NetworkInformationHandler(object sender, NetworkInformationEventArgs e);

    /// <summary>
    /// This specialized version of the TimerThread class adds the ability to stop/start the inferior thread based on the network connectivity.
    /// </summary>
    public class NetworkTimerThread : TimerThread
    {
        string IpAddress { get; set; }
        protected NetworkTimerThread()
        {
            // get ipAddress
            IpAddress = Extensions.GetBoardIpAddress();

            // track network availability
            NetworkChange.NetworkAvailabilityChanged += NetworkChange_NetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged += NetworkChange_NetworkAddressChanged;
        }

        void NetworkChange_NetworkAddressChanged(object sender, EventArgs e)
        {
            // if somebody wanted to know the network changed...
            if (NetworkInformation != null)
            {
                var newIpAddress = Extensions.GetBoardIpAddress();
                if (newIpAddress != IpAddress)
                {
                    IpAddress = newIpAddress;
                    NetworkInformation(sender, new NetworkInformationEventArgs(true, IpAddress));
                }
            }
        }

        void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            Debug.Print("Network is available is " + e.IsAvailable.ToString());

            // restart the timer thread
            if (!e.IsAvailable)
                Reset();
            else
            {
                Start();

                // if somebody wanted to know the network changed...
                if (NetworkInformation != null)
                    NetworkInformation(sender, new NetworkInformationEventArgs(e.IsAvailable, IpAddress));
            }
        }

        public static event NetworkInformationHandler NetworkInformation;
    }
}
