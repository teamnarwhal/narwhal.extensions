//-----------------------------------------------------------------------------
// These sources were originally authored by Huysentruit Wouter for his
// company Fastload Media.
//
// See http://www.linkedin.com/in/whuysentruit for more information.
//
// Improvements made by The Narwhal Group.
//-----------------------------------------------------------------------------
using System;
using System.Reflection;
using NetMf.CommonExtensions;

namespace FastloadMedia.NETMF.Http
{
    /// <summary>
    /// This static class contains a method to convert a value to its JSON equivalent.
    /// Programmed by Huysentruit Wouter
    /// </summary>
    public static class Json
    {
        /// <summary>
        /// Lookup table for hex values.
        /// </summary>
        private const string HEX_CHARS = "0123456789ABCDEF";
        public const string ContentType = "application/json";

        /// <summary>
        /// Converts a character to its javascript Unicode equivalent.
        /// </summary>
        /// <param name="c">The character to convert.</param>
        /// <returns>The javascript Unicode equivalent.</returns>
        private static string JsUnicode(char c)
        {
            string result = "\\u";
            ushort value = (ushort)c;

            for (int i = 0; i < 4; i++, value <<= 4)
                result += HEX_CHARS[value >> 12];

            return result;
        }

        /// <summary>
        /// Encodes a javascript string.
        /// </summary>
        /// <param name="s">The string to encode.</param>
        /// <returns>The encoded string.</returns>
        private static string JsEncode(string s)
        {
            string result = "";

            foreach (char c in s)
            {
                if (c < (char)127)
                {
                    switch (c)
                    {
                        case '\b': result += "\\b"; break;
                        case '\t': result += "\\t"; break;
                        case '\n': result += "\\n"; break;
                        case '\f': result += "\\f"; break;
                        case '\r': result += "\\r"; break;
                        case '"': result += "\\\""; break;
                        case '/': result += "\\/"; break;
                        case '\\': result += "\\\\"; break;
                        default:
                            if (c < ' ')
                                result += JsUnicode(c);
                            else
                                result += c;
                            break;
                    }
                }
                else
                    result += JsUnicode(c);
            }

            return result;
        }

        /// <summary>
        /// Convert a value to JSON.
        /// </summary>
        /// <param name="o">The value to convert. Supported types are: Boolean, String, Byte, (U)Int16, (U)Int32, Float, Double, Decimal, JsonObject, JsonArray, Array, Object and null.</param>
        /// <returns>The JSON object as a string or null when the value type is not supported.</returns>
        /// <remarks>For objects, only public fields are converted.</remarks>
        public static string ToJson(object o)
        {
            if (o == null)
                return "null";

            Type type = o.GetType();
            switch (type.Name)
            {
                case "Boolean":
                    return "\"" + ((bool)o ? "true" : "false") + "\"";
                case "String":
                    return "\"" + JsEncode((string)o) + "\"";
                case "DateTime":
                    return "\"" + JsEncode(((DateTime)o).ToString("s") + "Z") + "\"";
                case "Byte":
                case "Int16":
                case "UInt16":
                case "Int32":
                case "UInt32":
                case "Single":
                case "Double":
                case "Decimal":
                case "JsonObject":
                    return "\"" + o.ToString() + "\"";
                case "JsonArray":
                    return o.ToString();
            }

            if (type.IsArray)
            {
                var ja = new JsonArray((Array)o);
                return ja.ToString();
            }

            if (type.IsClass)
            {
                JsonObject jsonObject = new JsonObject(type.Name);
                foreach (FieldInfo info in type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
                {
                    if (info.FieldType.IsNotPublic)
                        continue;

                    if (info.GetValue(o) != null)
                    {
                        jsonObject.Add(info.FieldName(), info.GetValue(o));
                    }
                }
                return jsonObject.ToString();
            }

            // just give it a whirl, who knows it might work!
            return "\"" + o.ToString() + "\"";
        }
    }

    public static class FieldInfoExtensions
    {
        public static string FieldName(this FieldInfo fi)
        {
            var name = fi.Name;
            if (name[0] == '_') name = name.Substring(1);
            return name
                .Replace("k__BackingField", string.Empty)
                .Replace("<", string.Empty)
                .Replace(">", string.Empty);
        }
    }
}