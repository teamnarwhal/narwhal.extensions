﻿using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Net.NetworkInformation;

namespace Narwhal.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Calculate the number of milliseconds from the given time to the next interval.
        /// </summary>
        /// <remarks>Allows the caller to align periods on time boundaries. For example, to have a timer go off every 10 seconds at 00:00, 00:10, 00:20, 00:30, etc. </remarks>
        /// <param name="start">Starting time from which to calculate how long to the next interval</param>
        /// <param name="milliseconds">The interval duration, in milliseconds</param>
        /// <returns>TimeSpan containing the number of milliseconds to the next interval.</returns>
        public static TimeSpan TimespanToNextInterval(this TimeSpan ts, int milliseconds)
        {
            // calculate how many milliseconds since midnight
            var start = new DateTime(ts.Ticks);
            var midnight = (start - new DateTime(start.Year, start.Month, start.Day)).Ticks / TimeSpan.TicksPerMillisecond;

            // the difference between the interval and midnight MOD the interval 
            // is how many milliseconds from start the next interval will be
            return new TimeSpan(0, 0, 0, 0, milliseconds - (int)(midnight % milliseconds));
        }

        public static string ISO8601ToString(this DateTime dt)
        {
            return dt.ToString("s") + "Z";
        }

        public static string GetBoardIpAddress()
        {
            var ipAddress = "0.0.0.0";
            try
            {
                ipAddress = NetworkInterface.GetAllNetworkInterfaces()[0].IPAddress;
            }
            catch { }
            return ipAddress;
        }
    }
}
