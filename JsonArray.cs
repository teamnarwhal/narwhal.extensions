//-----------------------------------------------------------------------------
// These sources were originally authored by Huysentruit Wouter for his
// company Fastload Media.
//
// See http://www.linkedin.com/in/whuysentruit for more information.
//
// Improvements made by The Narwhal Group.
//-----------------------------------------------------------------------------
using System.Collections;
using System.Text;

namespace FastloadMedia.NETMF.Http
{
    /// <summary>
    /// A Json Array.
    /// Programmed by Huysentruit Wouter
    /// 
    /// Supported value types are those supported by the Json.ToJson method.
    /// See the Json.ToJson method for more information.
    /// </summary>
    public class JsonArray : ArrayList
    {
        public JsonArray()
        {
        }
        public JsonArray(object[] objects)
        {
            foreach (var o in objects)
                Add(o);
        }
        public JsonArray(System.Array array)
        {
            foreach (object o in array)
                Add(o);
        }

        /// <summary>
        /// Convert the array to its JSON representation.
        /// </summary>
        /// <returns>A string containing the JSON representation of the array.</returns>
        public override string ToString()
        {
            var parts = new string[Count];

            for (int i = 0; i < Count; i++)
            {
                if (this[i] is JsonObject)
                    parts[i] = (this[i] as JsonObject).ToString();
                else
                    parts[i] = Json.ToJson(this[i]);
            }
            var sb = new System.Text.StringBuilder();

            foreach (string part in parts)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(part);
            }

            return "[" + sb.ToString() + "]";
        }
    }
}
