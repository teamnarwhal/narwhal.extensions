//-----------------------------------------------------------------------------
// These sources were originally authored by Huysentruit Wouter for his
// company Fastload Media.
//
// See http://www.linkedin.com/in/whuysentruit for more information.
//
// Improvements made by The Narwhal Group.
//-----------------------------------------------------------------------------
using System;
using System.Text;
using System.Collections;

namespace FastloadMedia.NETMF.Http
{
    /// <summary>
    /// A Json Object.
    /// Programmed by Huysentruit Wouter
    /// 
    /// Keys must be strings!
    /// Supported value types are those supported by the Json.ToJson method.
    /// See the Json.ToJson method for more information.
    /// </summary>
    public class JsonObject : Hashtable
    {
        /// <summary>
        /// Convert the object to its JSON representation.
        /// </summary>
        /// <returns>A string containing the JSON representation of the object.</returns>
        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();

            string[] keys = new string[Count];
            object[] values = new object[Count];

            Keys.CopyTo(keys, 0);
            Values.CopyTo(values, 0);

            for (int i = 0; i < Count; i++)
            {
                string value = Json.ToJson(values[i]);
                if (value == string.Empty || value == null)
                    continue;

                if (sb.Length > 0)
                    sb.Append(", ");

                sb.Append("\"");
                sb.Append(keys[i]);
                sb.Append("\"");
                sb.Append(" : ");
                sb.Append(value);
            }

            return (Name != string.Empty ? "{ \"" + Name + "\" :" : string.Empty)
                + "{" + sb.ToString() + "}"
                + (Name != string.Empty ? "}" : string.Empty);
        }

        public string Name = string.Empty;
        public JsonObject()
        {
        }
        public JsonObject(string name)
        {
            Name = name;
        }
        public JsonObject(string Name, params object[] args)
            : this(Name)
        {
            if (args == null || args.Length == 0)
                return;

            if (args.Length % 2 != 0)
                throw new ArgumentException("Must pass pairs of arguments");

            for (int n = 0; n < args.Length; n += 2)
            {
                if (args[n].GetType() != typeof(string))
                    throw new ArgumentException(args[n].ToString() + " not passed as a string type");

                Add(args[n], args[n + 1]);
            }
        }
    }
}
